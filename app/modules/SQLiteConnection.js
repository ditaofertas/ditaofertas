import SQLite from 'react-native-sqlite-storage';

let db = null;

export function SQLiteConnection() {
  function getInstance() {
    if (db === null) {
      db = SQLite.openDatabase(
        "ditaOfertas.db",
        "1.0",
        "Dita Ofertas Database",
        200000,
        openDB,
        errorDB);
    }
    return db;
  };

  function openDB() {
    console.log('DB OPENED');
  }

  function errorDB(error) {
    console.log('DB ERROR', error);
  }

  return getInstance();
}
