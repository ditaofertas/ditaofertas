
import * as types from '../constants';

const initialState = {
  networkConnected: true,
  startingApp: true,
  appConfigured: false,
  appIsOnBackground: false,
  TTSConfigured: false,
  currentScreen: '',
  navigationStack: [],
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.NAVIGATE_TO_SCREEN:
      return {
        ...state,
        currentScreen: payload,
        navigationStack: [
          ...state.navigationStack,
          payload,
        ],
      };

    case types.APP_IS_ON_BACKGROUND:
      return {
        ...state,
        appIsOnBackground: true,
      };

    case types.BACK_SCREEN: {
      if (!state.navigationStack.length) {
        return state;
      }

      const stack = state.navigationStack.length > 1
        ? state.navigationStack.slice(0, state.navigationStack.length - 1)
        : state.navigationStack;
      const screen = stack.slice(stack.length - 1)[0];
      return {
        ...state,
        currentScreen: screen,
        navigationStack: stack,
      };
    }

    case types.TTS_CONFIGURED:
      return {
        ...state,
        TTSConfigured: true,
      };

    case types.START_APP_SUCCESS:
      return {
        ...state,
        startingApp: false,
        appConfigured: true,
      };

    case types.START_APP_ERROR:
      return {
        ...state,
        startingApp: false,
        appConfigured: false,
      };

    case types.CREATE_TABLES_ERROR:
      return {
        ...state,
        startingApp: false,
      };

    default:
      return state;
  }
};
