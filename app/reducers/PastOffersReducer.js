
import * as types from '../constants';

const initialState = {
  speakingState: {
    started: false,
    stopped: true,
    finished: true,
  },
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.APP_IS_ON_BACKGROUND:
      return initialState;

    case types.SPEAK_STARTED_PAST_OFFERS: {
      const { speakingState } = state;
      return {
        ...state,
        speakingState: {
          ...speakingState,
          started: true,
          stopped: false,
          finished: false,
        },
      };
    }

    case types.SPEAK_FINISHED_PAST_OFFERS: {
      const { speakingState } = state;
      return {
        ...state,
        speakingState: {
          ...speakingState,
          started: false,
          finished: true,
        },
      };
    }

    case types.SPEAK_STOPPED_PAST_OFFERS: {
      const { speakingState } = state;
      return {
        ...state,
        speakingState: {
          ...speakingState,
          started: false,
          stopped: true,
          finished: true,
        },
      };
    }

    default:
      return state;
  }
};
