import { combineReducers } from 'redux';
import App from './AppReducer';
import Main from './MainReducer';
import NewProfile from './NewProfile';
import PastOffers from './PastOffersReducer';

export default combineReducers({
  App,
  Main,
  NewProfile,
  PastOffers,
});
