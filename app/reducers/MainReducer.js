
import * as types from '../constants';

const initialState = {
  fetchingOffers: false,
  offersFetched: false,
  offers: [],
  productToPlay: null,
  indexOffer: 0,
  speakingState: {
    started: false,
    stopped: true,
    finished: true,
  },
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.APP_IS_ON_BACKGROUND:
      return initialState;

    case types.FETCH_OFFERS:
      return {
        ...state,
        fetchingOffers: true,
      };

    case types.FETCH_OFFERS_SUCCESS: {
      if (!payload.data.length) {
        return {
          ...state,
          fetchingOffers: false,
        };
      }
      return state;
    }

    case types.SAVE_OFFERS_SEARCHED_SUCCESS: {
      const offers = payload.data;
      return {
        ...state,
        offers: offers.slice(0, 3).map(offer => ({ // slice para teste
          ...offer,
          played: false,
        })),
        productToPlay: {
          ...offers[state.indexOffer],
        },
        indexOffer: state.indexOffer + 1,
        fetchingOffers: false,
        offersFetched: true,
      };
    }

    case types.SAVE_OFFERS_SEARCHED_ERROR:
      return {
        ...state,
        fetchingOffers: false,
      };

    case types.SPEAK_STARTED: {
      const { speakingState } = state;
      return {
        ...state,
        speakingState: {
          ...speakingState,
          started: true,
          stopped: false,
          finished: false,
        },
      };
    }

    case types.SPEAK_FINISHED: {
      const { speakingState, indexOffer, offers } = state;

      if (indexOffer >= offers.length) {
        return initialState;
      }

      return {
        ...state,
        speakingState: {
          ...speakingState,
          started: false,
          finished: true,
        },
        offers: [
          ...offers.slice(0, indexOffer),
          {
            ...offers[indexOffer],
            played: true,
          },
          ...offers.slice(indexOffer + 1),
        ],
        productToPlay: indexOffer <= offers.length ? {
          ...offers[indexOffer],
        } : null,
        indexOffer: indexOffer + 1,
      };
    }

    case types.SPEAK_STOPPED:
      return initialState;

    default:
      return state;
  }
};
