
import * as types from '../constants';

const initialState = {
  savingProfile: false,
  stores: [],
  categories: [],
  profiles: [],
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case types.SAVE_CATEGORIES_STORES_SUCCESS: {
      const { categories, stores } = payload;
      return {
        ...state,
        categories,
        stores,
      };
    }

    case types.FETCH_PROFILES_SUCCESS: {
      const profilesList = payload.map(profile => ({
        ...profile,
        categories: JSON.parse(profile.categories),
        stores: JSON.parse(profile.stores),
      }));

      return {
        ...state,
        savingProfile: false,
        profiles: [
          ...profilesList,
        ],
      };
    }

    case types.SAVE_PROFILE:
      return {
        ...state,
        savingProfile: true,
      };

    case types.SAVE_PROFILE_SUCCESS: {
      const { profiles } = state;
      return {
        ...state,
        savingProfile: false,
        profiles: [
          ...profiles,
          payload,
        ],
      };
    }

    case types.SAVE_PROFILE_ERROR:
      return {
        ...state,
        savingProfile: false,
      };

    case types.REMOVE_PROFILE_SUCCESS:
      return {
        ...state,
        profiles: [
          ...state.profiles.slice(0, payload.index),
          ...state.profiles.slice(payload.index + 1),
        ],
      };

    default:
      return state;
  }
};
