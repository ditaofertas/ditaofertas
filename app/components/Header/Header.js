import React, { PropTypes } from 'react';
import { View } from 'react-native';

const DtHeader = ({ title }) => (
  <View>
    Header
  </View>
);

DtHeader.defaultProps = {
  title: 'Dita Ofertas',
};

DtHeader.propTypes = {
  title: PropTypes.string.isRequerid,
};

export default DtHeader;
