import React from 'react';
import { View, ScrollView, StyleSheet } from 'react-native';

const defaultStyle = StyleSheet.create({
  container: {
    padding: 10,
  }
});

const Container = ({ children, style, scroll, ...props }) => {
  const Element = scroll ? ScrollView : View;
  return (
    <Element style={[defaultStyle.container, style]} {...props}>
      {children}
    </Element>
  );
};

export default Container;
