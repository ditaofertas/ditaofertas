import React, { PropTypes, Component } from 'react';
import { Modal as NativeModal, View, TouchableHighlight, Text } from 'react-native';

class Modal extends Component {

  constructor(props) {
    super(props);
    this.state = {
      open: props.open,
    };
  }

  componentWillReceiveProps(nextProps) {
    this.setState((state) => ({
      ...state,
      ...nextProps,
    }));
  }

  render() {
    return (
      <NativeModal
        animationType={"slide"}
        transparent={false}
        visible={this.state.open}
        onRequestClose={() => {alert("Modal has been closed.")}}
        >
       <View style={{marginTop: 22}}>
        <View>
          <Text>Hello World!</Text>

          <TouchableHighlight onPress={() => {
            this.setState({open: false});
          }}>
            <Text>Hide Modal</Text>
          </TouchableHighlight>

        </View>
       </View>
      </NativeModal>
    );
  }
}

Modal.propTypes = {
  open: PropTypes.bool,
};

export default Modal;
