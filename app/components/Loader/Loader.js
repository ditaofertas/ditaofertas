import React, { Component, PropTypes } from 'react';
import ReactNative from 'react-native';
import Container from './../../components/Container';

const {
  Text,
  View,
  StyleSheet,
  ActivityIndicator,
} = ReactNative;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'powderblue',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },

  text: {
    fontSize: 20,
    textAlign: 'center',
  },
});

const Loader = ({ text }) => (
  <Container style={styles.container}>
    <View>
      <ActivityIndicator
        animating
        size={40}
      />
      <Text style={styles.text}>{text}</Text>
    </View>
  </Container>
);

Loader.defaultProps = {
  text: 'Iniciando a aplicação...',
};

Loader.propTypes = {
  text: PropTypes.string,
};

export default Loader;
