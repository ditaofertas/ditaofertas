import React, { PropTypes, Component } from 'react';
import { TouchableHighlight, View, Text, } from 'react-native';

import Item from './Item';
import styles from './styles';

class ListItems extends Component {

  constructor(props) {
    super(props);

    this.state = {
      list: props.items,
    };
  }

  componentWillReceiveProps(nextProps) {
    const { items } = nextProps;
    this.setState({
      list: items,
    });
  }

  toggleItem = (item, index) => {
    const { action, field, multiple } = this.props;
    const itemActive = {
      ...item,
      checked: !item.checked,
    };

    const newList = this.state.list.map((listItem) => {
      if (listItem[field] === item[field]) {
        return {
          ...listItem,
          checked: !listItem.checked,
        };
      } else if (!multiple) {
        return {
          ...listItem,
          checked: false,
        }
      }

      return listItem;
    })

    this.setState({
      list: newList,
    }, () => {
      if (multiple) {
        action(newList, index);
      } else {
        action(newList, index, itemActive);
      }
    });
  }

  render() {
    const { list } = this.state;
    return (
      <View style={styles.listItem}>
        {
          list.map((item, index) => (
            <Item
              key={index}
              index={index}
              item={item}
              action={this.toggleItem}
              handleRemoveItem={() => this.props.removeItem(item.dsName, index)}
              checked={item.checked}
              removeOption={this.props.removeOption}
            />))
        }
      </View>
    );
  }
}

ListItems.defaultProps = {
  multiple: true,
  field: 'dsName',
};

ListItems.propTypes = {
  items: PropTypes.array,
  action: PropTypes.func,
  removeItem: PropTypes.func,
  field: PropTypes.string,
  multiple: PropTypes.bool,
  removeOption: PropTypes.bool,
};


export default ListItems;
