import React, { PropTypes, Component } from 'react';
import { 
  TouchableHighlight,
  View,
  Text,
  Switch,
  Image,
} from 'react-native';

import styles from './styles';
import Assets from './../../assets';

class Item extends Component {
  constructor(props) {
    super(props);

    this.state = {
      checked: props.checked,
    };
  }

  componentWillReceiveProps(nextProps) {
    const { checked } = nextProps;
    this.setState((item) => ({
      ...item,
      checked,
    }));
  }

  handleAction = () => {
    const { item, index, action } = this.props;

    this.setState((state) => ({
      checked: !this.state.checked,
    }), action(item, index));
  }

  render() {
    const { item, action, removeOption } = this.props;
    const { checked } = this.state;
    return (
      <TouchableHighlight
        underlayColor="transparent"
        onPress={this.handleAction}
      >
        <View style={styles.item}>
          {
            removeOption &&
            <TouchableHighlight
              underlayColor="transparent"
              onPress={this.props.handleRemoveItem}
              style={{ width: 40 }}
            >
              <Image source={Assets.images.trash} />
            </TouchableHighlight>
          }
          <View style={styles.itemContent}>
            <Text style={styles.itemName}>{ item.dsName }</Text>
            <View style={styles.checkbox}>
              <Switch value={checked} onValueChange={this.handleAction} />
            </View>
          </View>
        </View>
      </TouchableHighlight>
    );
  }
}

Item.defaultProps = {
  removeOption: false,
}

Item.propTypes = {
  index: PropTypes.number,
  item: PropTypes.object,
  action: PropTypes.func,
  handleRemoveItem: PropTypes.func,
  checked: PropTypes.bool,
  removeOption: PropTypes.bool,
};

export default Item;
