import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  item: {
    paddingTop: 10,
    paddingBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  itemContent: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  itemName: {
    fontSize: 16,
  },
  checkbox: {
    alignSelf: 'flex-end',
    width: 100,
  },
});

export default styles;
