import React, { PropTypes } from 'react';
import ReactNative, { ToastAndroid } from 'react-native';
import Container from './../../components/Container';
import Assets from './../../assets';

const {
  Text,
  View,
  StyleSheet,
  Image,
  Linking,
  TouchableHighlight,
} = ReactNative;

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'powderblue',
    flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
  },

  content: {
    flex: 1,
    flexDirection: 'column',
  },

  label: {
    fontSize: 18,
    fontWeight: 'bold',
  },

  text: {
    fontSize: 20,
    textAlign: 'center',
  },

  texURL: {
    fontSize: 20,
    textDecorationLine: 'underline',
  },

  textPrice: {
    fontSize: 22,
    textAlign: 'center',
    fontWeight: 'bold',
  },

  categoryTitle: {
    height: 120,
    flexDirection: 'column',
    justifyContent: 'space-between',
    paddingLeft: 10,
    paddingRight: 10,
    paddingTop: 5,
    paddingBottom: 5,
    backgroundColor: '#6495ed',
  },

  categoryTitleItem: {
    flexDirection: 'column',
  },

  categoryLabel: {
    marginRight: 5,
  },

  categoryValue: {
    fontSize: 20,
    color: '#fff',
  },

  row: {
    paddingTop: 2,
    paddingBottom: 2,
    marginBottom: 10,
    borderBottomColor: '#6495ed',
  },

  pauseButton: {
    paddingTop: 5,
    paddingBottom: 5,
    paddingLeft: 10,
    paddingRight: 10,
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#6495ed',
  },
});

const handleClick = (url) => {
  Linking.canOpenURL(url).then(supported => {
    if (supported) {
      Linking.openURL(url);
    } else {
      ToastAndroid.show('Impossível abrir essa URL', ToastAndroid.SHORT);
    }
  });
};

const Player = ({ offer, stopAction }) => {
  const { dsName, dsURL, dsPrice, categoryName, storeName } = offer;
  const { stop } = Assets.images;
  return (
    <Container style={styles.container}>
      <View style={styles.content}>
        <View style={styles.categoryTitle}>
          <View style={styles.categoryTitleItem}>
            <Text style={styles.categoryLabel}>Loja:</Text>
            <Text style={styles.categoryValue}>{storeName}</Text>
          </View>
          <View style={styles.categoryTitleItem}>
            <Text style={styles.categoryLabel}>Categoria:</Text>
            <Text style={styles.categoryValue}>{categoryName.toUpperCase()}</Text>
          </View>
        </View>

        <View style={styles.row}>
          <Text style={styles.label}>Produto:</Text>
          <Text style={styles.text}>{dsName}</Text>
        </View>

        <View style={styles.row}>
          <Text style={styles.label}>Preço:</Text>
          <Text style={styles.textPrice}>{dsPrice}</Text>
        </View>

        <View style={styles.row}>
          <Text style={styles.label}>URL:</Text>
          <Text style={styles.texURL} onPress={() => handleClick(dsURL)} >{dsURL}</Text>
        </View>

        <TouchableHighlight
          onPress={stopAction}
          underlayColor="transparent"
          style={styles.pauseButton}
        >
          <Image source={stop} />
        </TouchableHighlight>
      </View>
    </Container>
  );
};

Player.propTypes = {
  offer: PropTypes.object.isRequired,
  stopAction: PropTypes.func.isRequired,
};

export default Player;
