import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'powderblue',
  },

  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'stretch',
    flexWrap: 'wrap',
  },

  item: {
    flex: 1,
    alignSelf: 'stretch',
    alignItems: 'center',
  },

  itemNewProfile: {
    backgroundColor: 'skyblue',
  },

  itemOldOffers: {
    backgroundColor: 'steelblue',
  },

  button: {
    alignItems: 'center',
  },

  text: {
    color: '#fff',
    fontSize: 16,
    textAlign: 'center',
  },

  titleListProfiles: {
    padding: 10,
    backgroundColor: '#4169e1',
    color: '#fff',
    fontSize: 20,
  },
});

export default styles;
