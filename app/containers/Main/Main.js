import React, { PropTypes, Component } from 'react';
import ReactNative, { ToastAndroid } from 'react-native';
import TTS from 'react-native-tts';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import  * as actions from './../../actions/AppActions';
import * as sql from './../../API/SQL';

import Container from './../../components/Container';
import ListItem from './../../components/ListItems'
import Loader from './../../components/Loader';
import Player from './../../components/Player';
import Assets from './../../assets';

import styles from './styles';

const {
  StyleSheet,
  Image,
  TouchableHighlight,
  View,
  ScrollView,
  Button,
  Text,
  Alert,
} = ReactNative;

class Main extends Component {

  constructor(props) {
    super(props);

    this.state = {
      listProfiles: [],
      selectedProfile: null,
      productToPlay: null,
      currentScreen: 'Main',
      speakStarted: false,
    }
  }

  componentWillMount() {
    if (this.props.currentScreen !== this.state.currentScreen) {
      this.props.actions.navigateScreen('Main');
    }
  }

  componentDidMount() {
    if (!this.props.TTSConfigured) {
      TTS.addEventListener('tts-start', this.voiceStarted);
      TTS.addEventListener('tts-finish', this.voiceFinished);
      TTS.setDefaultLanguage('pt-BR');
      TTS.setDefaultRate(0.4);
      this.props.actions.TTSConfigured();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.offersFetched && nextProps.productToPlay && !nextProps.speakingState.started) {
      this.setState({
        productToPlay: nextProps.productToPlay,
        speakStarted: true,
      }, () => this.handleSayTTS())
    } else if (!nextProps.productToPlay) {
      this.setState({
        productToPlay: null,
        speakStarted: false,
      })
    }

    if (this.state.speakStarted && nextProps.speakingState.stopped) {
      this.setState({
        productToPlay: null,
        speakStarted: false,
      })
    }

    this.setState({
      listProfiles: [
        ...nextProps.profiles,
      ]
    })
  }

  componentWillUnmount() {
    this.clearTTS();
  }

  clearTTS = () => {
    this.handleStopTTS();
    TTS.removeEventListener('tts-start', this.voiceStarted);
    TTS.removeEventListener('tts-finish', this.voiceFinished);
  }

  resetState = (callback) => {
    if (!!this.state.selectedProfile || !!this.state.productToPlay) {
      this.setState(state => ({
        ...state,
        selectedProfile: null,
        productToPlay: null,
      }), () => callback());
    } else {
      callback();
    }
  }

  isOnScreen = (props) => {
    return props.currentScreen === this.state.currentScreen;
  };

  speak = (product) => {
    TTS.speak(`${product.dsName} ${product.dsPrice}`);
  }

  voiceStarted = () => {
    if (this.isOnScreen(this.props)) {
      if (!this.props.speakingState.started) {
        this.props.actions.speakStarted()
      }
    }
  }

  voiceFinished = () => {
    if (this.isOnScreen(this.props)) {
      const { speakingState } = this.props;
    
      if (!speakingState.stopped) {
        this.handleStopTTS();
        this.props.actions.speakFinished();
      }
    }
  }

  goToNewProfile = () => {
    const { actions, categories, stores } = this.props;
    const { navigate } = this.props.navigation;
    this.resetState(() => navigate('NewProfile'));
  }

  goToOldOffers = () => {
    const { navigate } = this.props.navigation;
    this.resetState(() => navigate('PastOffers'));
  }

  handleSayTTS = () => {
    if (this.state.productToPlay) {
      this.speak(this.state.productToPlay);
    }else {
      this.props.actions.speakStopped();
    }
  }

  handleStopTTS = () => {
    TTS.stop();
  }

  handleStopPlayer = () => {
    this.props.actions.speakStopped();
    this.handleStopTTS()
  }

  handleSayOffers = () => {
    if (!this.state.selectedProfile) {
      ToastAndroid.show('Nenhum perfil selecionado', ToastAndroid.SHORT);
    } else {
      const { fetchOffers, saveOffersSearched } = this.props.actions;
      const categories = this.state.selectedProfile.categories.map(category => category.id);
      const stores = this.state.selectedProfile.stores.map(store => store.id);

      fetchOffers({ categories, stores }).then((response) => {
        if (!response.data.data.length) {
          ToastAndroid.show('Nenhuma oferta encontrada!', ToastAndroid.SHORT);
        } else {
          saveOffersSearched({
            dsName: this.state.selectedProfile.dsName,
            offers: response.data,
          })
        }
      }).catch(err => console.error(err));
    }
  }

  handleSelectProfile = (profile, index) => {
    this.setState((state) => ({
      ...state,
      selectedProfile: {
        ...profile[index],
        index
      },
    }));
  }

  handleRemoveProfile = (dsName, index) => {
    this.props.actions.removeProfile(dsName, index)
    console.log('handleRemoveProfile', dsName, index)
    this.setState({
      listProfiles: [
        ...this.state.listProfiles.slice(0, index),
        ...this.state.listProfiles.slice(index + 1),
      ]
    }, () => console.log('perfil removido', this.state))
  }

  showMessage = (dsName, index) => {
    Alert.alert(
      'Excluir perfil',
      'Você tem certeza que deseja excluir este perfil?',
      [
        { text: 'Não', onPress: () => console.log('Não') },
        { text: 'Sim', onPress: () => this.handleRemoveProfile(dsName, index) },
      ])
  }

  getProfileList = () => {
    console.log('profiles props', this.props.profiles)
    console.log('profiles state', this.state.listProfiles)
    // Passo a lista de perfis que vem do redux como prop
    const newListProfiles = this.state.listProfiles.map((profile, index) => {
      if (this.state.selectedProfile && this.state.selectedProfile.index === index) {
        return {
          ...profile,
          checked: true,
        }
      }

      return {
        ...profile,
        checked: false,
      }
    });
    return (
      <ListItem
        items={newListProfiles}
        action={this.handleSelectProfile}
        removeItem={this.showMessage}
        multiple={false}
        removeOption
      />
    );
  }

  getButtonActions = (image, text, action) => {
    return (
      <TouchableHighlight
        onPress={action}
        underlayColor="transparent"
        style={{padding: 10, alignSelf: 'stretch'}}
      >
        <View style={{alignItems: 'center', alignSelf: 'stretch'}}>
          { image ? <Image source={image} /> : null }
          <Text style={styles.text}>{ text }</Text>
        </View>
      </TouchableHighlight>
    );
  }

  checkTables = () => {
    sql.checkTables()
      .catch(err => console.error(err));
  }

  dropTables = () => {
    const dropApp = sql.dropTableApp();
    const dropCat = sql.dropTableCategories();
    const dropStr = sql.dropTableStores();
    const dropProf = sql.dropTableProfile();
    const dropPastOffers = sql.dropTablePastOffers();

    Promise.all([dropApp, dropCat, dropStr, dropProf, dropPastOffers])
      .then(response => {
      })
      .catch(err => console.error(err));
  }

  selectLastOffers = () => {
    sql.selectPastOffers().then(response => {
    }).catch(err => console.error(err));
  }

  renderComponent = () => {
    const { fetchingOffers, startingApp, speakingState } = this.props
    // Enquanto estiver buscando as ofertas, aparece a mensagem de carregando...
    if (fetchingOffers) {
      return <Loader text="Carregando as ofertas..." />
    } else if (startingApp) {
      return <Loader />
    } else if (!!this.state.productToPlay) {
      return <Player offer={this.state.productToPlay} stopAction={this.handleStopPlayer} />
    }

    return null;
  }

  render() {
    const {
      actions,
      navigation,
      startingApp,
      profiles,
    } = this.props;
    const { navigate } = navigation;
    // const { listProfiles } = this.state;
    const { profile: profileImg, mktList: mktListImg } = Assets.images;

    return (
      this.renderComponent() ||
      <Container scroll style={styles.container}>
        <View style={styles.row}>
          <View style={[styles.item, styles.itemNewProfile]}>
            { this.getButtonActions(profileImg, 'Novo Perfil', this.goToNewProfile) }
          </View>
          <View style={[styles.item, styles.itemOldOffers]}>
            { this.getButtonActions(mktListImg, 'Buscas Anteriores', this.goToOldOffers) }
          </View>
        </View>
        {
          profiles.length ?
            <ScrollView style={{marginTop: 30}}>
              <View>
                <Text style={styles.titleListProfiles}>Perfis salvos</Text>
                { this.getProfileList() }
              </View>
            </ScrollView>
          : <View style={{ paddingTop: 10, paddingBottom: 10 }}>
              <Text>Você deve criar um perfil...</Text>
            </View>
        }
        <View style={{marginTop: 10}}>
          <Button
            onPress={this.handleSayOffers}
            title="Ditar Ofertas"
          />
        </View>
        {
          __DEV__ && 
          <View>
            <View style={{marginTop: 10, marginBottom: 10}}>
              <Button
                onPress={this.dropTables}
                color="#dc143c"
                title="Limpar Tabelas - DEV"
              />
            </View>
            <View style={{marginTop: 10, marginBottom: 40}}>
              <Button
                onPress={this.checkTables}
                color="#dc143c"
                title="SELECT Tabelas - DEV"
              />
            </View>

            <View style={{marginTop: 10, marginBottom: 40}}>
              <Button
                onPress={this.selectLastOffers}
                color="#dc143c"
                title="SELECT Last Offers - DEV"
              />
            </View>
          </View>
        }
      </Container>
    );
  }
}

Main.propTypes = {
  currentScreen: PropTypes.string,
  profiles: PropTypes.array,
  speakingState: PropTypes.object,
  startingApp: PropTypes.bool,
  TTSConfigured: PropTypes.bool,
  fetchingOffers: PropTypes.bool,
  offersFetched: PropTypes.bool,
  productToPlay: PropTypes.object,
}

Main.defaultProps = {
  fetchingOffers: false,
  offersFetched: false,
}

const mapStateToProps = ({ App, Main, NewProfile }) => {
  const { startingApp, currentScreen, TTSConfigured } = App;
  const { speakingState, fetchingOffers, offersFetched, productToPlay } = Main;
  const { profiles } = NewProfile;

  return {
    startingApp,
    TTSConfigured,
    speakingState,
    profiles,
    currentScreen,
    fetchingOffers,
    offersFetched,
    productToPlay,
  }
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(actions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(Main);
