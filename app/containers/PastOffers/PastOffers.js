import React, { PropTypes, Component } from 'react';
import ReactNative, { ToastAndroid } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import TTS from 'react-native-tts';
import moment from 'moment';
import ptLocale from 'moment/locale/pt-br';
import  * as actions from './../../actions/AppActions';
import * as sql from '../../API/SQL';

import ToastModuleAndroid from './../../modules/Toast';
import Container from './../../components/Container';
import PastOffersItem from './PastOffersItem';
import Loader from './../../components/Loader';
import Player from './../../components/Player';

import styles from './styles';

const {
  View,
  ScrollView,
  Text,
  TextInput,
  Button,
  Modal,
  Alert,
} = ReactNative;

class PastOffers extends Component {

  constructor(props) {
    super(props);
    this.offers = [];

    this.state = {
      offers: [],
      selectedProfile: null,
      productToPlay: null,
      currentScreen: 'PastOffers',
    };
  }

  componentWillMount() {
    this.props.actions.navigateScreen('PastOffers');
  }

  componentDidMount() {
    moment.updateLocale('pt-br', ptLocale);
    TTS.setDefaultLanguage('pt-BR');
    TTS.setDefaultRate(0.4);
    TTS.addEventListener('tts-start', this.voiceStarted);
    TTS.addEventListener('tts-finish', this.voiceFinished);

    sql.selectPastOffers().then(response => {
      console.log('selectPastOffers ', response)
      this.setState({
        offers: response.map(item => {
          const dateSearched = moment(item.searchedAt);
          return {
            ...item,
            offers: JSON.parse(item.offers),
            searchedAt: dateSearched.calendar(),
            selected: false,
          }
        })
      })
    })
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (!this.isOnScreen(nextProps)) {
      return false;
    }

    return true;
  }

  componentWillUnmount() {
    this.clearTTS()
  }

  componentWillReceiveProps(nextProps) {
    if (!!this.state.productToPlay && nextProps.speakingState.stopped) {
      this.setState(state => ({
        ...state,
        productToPlay: null,
      }));
    }
  }

  clearTTS = () => {
    this.handleStopTTS();
    TTS.removeEventListener('tts-start', this.voiceStarted);
    TTS.removeEventListener('tts-finish', this.voiceFinished);
  }

  resetState = (callback) => {
    if (!!this.offers.length || !!this.state.selectedProfile || !!this.state.productToPlay) {
      this.offers = [];
      this.setState(state => ({
        ...state,
        selectedProfile: null,
        productToPlay: null,
      }), () => callback());
    } else {
      callback();
    }
  }

  isOnScreen = (props) => {
    return props.currentScreen === this.state.currentScreen;
  };

  speak = (product) => {
    this.setState((state) => ({
      ...state,
      productToPlay: product,
    }), () => TTS.speak(`${product.dsName} ${product.dsPrice}`));
  }

  voiceStarted = () => {
    if (this.isOnScreen(this.props)) {
      this.props.actions.speakStartedPastOffers();
    }
  }

  voiceFinished = () => {
    if (this.isOnScreen(this.props)) {
      const { speakingState } = this.props;
      const { productToPlay: processedProduct } = this.state;

      if (!speakingState.stopped) {
        this.offers = this.offers.map((product) => {
          if (product.id === processedProduct.id) {
            return {
              ...product,
              played: true,
            }
          }

          return product;
        });

        this.handleStopTTS();
        this.props.actions.speakFinishedPastOffers();
        this.getProductsList();
      }
    }
  }

  handleSelectProfile = (profile, index) => {
    this.setState((state) => ({
      ...state,
      selectedProfile: profile,
      offers: state.offers.map((offer, i) => {
        if (i === index ) {
          return {
            ...offer,
            selected: true,
          }
        }
        return {
          ...offer,
          selected: false,
        }
      })
    }));
  }

  handleRemoveItem = (index, itemId) => {
    this.props.actions.removeOffersSearched(itemId)
    this.setState((state) => ({
      ...state,
      offers: [
        ...state.offers.slice(0, index),
        ...state.offers.slice(index + 1),
      ],
    }));
  }

  showMessage(index, itemId) {
    Alert.alert(
      'Excluir registro',
      'Você tem certeza que deseja excluir este item?',
      [
        { text: 'Não', onPress: () => console.log('Não') },
        { text: 'Sim', onPress: () => this.handleRemoveItem(index, itemId) },
      ])
  }

  getProductsList = () => {
    console.log('playProductList');
    const { started, stopped, finished } = this.props.speakingState;
    if (!started && finished || stopped ) {
      const product = this.offers.find(prod => !prod.played);
      // console.table(this.offers);
      console.log('product', product);
      if (product) {
        this.speak(product);
      } else {
        this.props.actions.speakStoppedPastOffers();
      }
    }
  }

  handleSayTTS = () => {
    console.log('handleSayTTS');
    if (this.state.selectedProfile) {
      //slice só pra teste
      this.offers = this.state.selectedProfile.offers.data.slice(0,5);
      this.getProductsList();
    } else {
      ToastAndroid.show('Nenhum item selecionado', ToastAndroid.SHORT);
    }
  }

  handleStopTTS = () => {
    TTS.stop();
  }

  handleStopPlayer = () => {
    this.props.actions.speakStoppedPastOffers();
    TTS.stop();
  }

  renderComponent = () => {
    console.log('renderComponent', this.state);
    if (!!this.state.productToPlay && !this.props.speakingState.stopped) {
      console.log(this.state.productToPlay);
      return <Player offer={this.state.productToPlay} stopAction={this.handleStopPlayer} />
    }

    return null;
  }

  render() {
    return (
      this.renderComponent() ||
      <Container scroll>
        <ScrollView>
          <View style={styles.listContainer}>
            <Text style={styles.listTitle}>Perfis buscados</Text>
            {
              !!this.state.offers.length ?
              this.state.offers.map((item, i) => (
                  <PastOffersItem
                    key={i}
                    index={i}
                    profile={item}
                    selected={item.selected}
                    onPress={this.handleSelectProfile}
                    handleRemoveItem={() => this.showMessage(i, item.id)}
                  />
              ))
              : <Text>Nenhuma busca foi efetuada ainda...</Text>
            }
          </View>
          {
            !!this.state.offers.length ?
            <View style={{marginTop: 20, marginBottom: 20}}>
              <Button
                disabled={!this.state.offers.length}
                onPress={this.handleSayTTS}
                title="Ouvir novamente"
              />
            </View>
            : null
          }
          
        </ScrollView>
      </Container>
    );
  }
}

PastOffers.propTypes = {
  currentScreen: PropTypes.string,
  speakingState: PropTypes.object,
}

const mapStateToProps = ({ App, PastOffers }) => {
  const { currentScreen } = App;
  const { speakingState } = PastOffers;
  return {
    currentScreen,
    speakingState,
  }
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(actions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(PastOffers);
