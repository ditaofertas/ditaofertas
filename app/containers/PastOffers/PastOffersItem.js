import React, { PropTypes, Component } from 'react';

import {
  TouchableHighlight,
  View,
  Text,
  Switch,
  Image,
} from 'react-native';

import Assets from './../../assets';
import styles from './styles';

class PastOffersItem extends React.PureComponent {

  handleAction = () => {
    this.props.onPress(this.props.profile, this.props.index);
  }

  render() {
    const { dsName, searchedAt } = this.props.profile;
    return (
      <TouchableHighlight
        underlayColor="transparent"
        onPress={this.handleAction}
      >
        <View style={styles.item}>
          <TouchableHighlight
            underlayColor="transparent"
            onPress={this.props.handleRemoveItem}
            style={{ width: 40 }}
          >
            <Image source={Assets.images.trash} />
          </TouchableHighlight>
          <View style={{flex: 1}}>
            <Text>{ dsName }</Text>
            <Text>{ searchedAt }</Text>
          </View>
          <View style={styles.checkbox}>
            <Switch value={this.props.selected} onValueChange={this.handleAction} />
          </View>
        </View>
      </TouchableHighlight>
    );
  }
}

PastOffersItem.propTypes = {
  profile: PropTypes.object,
  index: PropTypes.number,
  onPress: PropTypes.func,
  handleRemoveItem: PropTypes.func,
};

export default PastOffersItem;
