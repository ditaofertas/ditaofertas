import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  listContainer: {

  },
  listTitle: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  item: {
    paddingTop: 10,
    paddingBottom: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  checkbox: {
    alignSelf: 'flex-end',
    width: 100,
  },
  btnSave: {
    marginTop: 50,
  },
});

export default styles;
