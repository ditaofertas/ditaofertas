import React, { PropTypes, Component } from 'react';
import ReactNative, { ToastAndroid } from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux'
import  * as actions from './../../actions/AppActions';

import Container from './../../components/Container';
import ListItem from './../../components/ListItems'
import Loader from './../../components/Loader';

import styles from './styles';

const {
  View,
  ScrollView,
  Text,
  TextInput,
  Button
} = ReactNative;

class NewProfile extends Component {

  constructor(props) {
    super(props);

    this.categoriesList = [];
    this.storesList = [];
    this.state = {
      dsName: '',
      categories: [],
      stores: [],
      savingProfile: props.savingProfile,
      currentScreen: props.currentScreen,
    };
  }

  /*componentWillReceiveProps(nextProps) {
    console.log('componentWillReceiveProps');
    console.log(nextProps);
    const { savingProfile } = nextProps;

    if (nextProps.currentScreen) {
      this.setState(state => ({
        ...state,
        currentScreen: nextProps.currentScreen,
      }))
    }
    
    this.setState((state) => ({
      ...state,
      savingProfile,
    }));
  }*/

  componentWillMount() {
    console.log('componentDidMount');
    console.log(this.props);
    this.props.actions.navigateScreen('NewProfile');
    /*const { state } = this.props.navigation;*/
    const { categories, stores } = this.props;

    this.categoriesList = categories.map(category => ({
      ...category,
      checked: false,
    }));

    this.storesList = stores.map(store => ({
      ...store,
      checked: false,
    }));
  }

  isOnScreen = () => {
    return this.state.currentScreen === 'NewProfile';
  };

  addCategory = (categoriesList) => {
    console.log('addCategory', categoriesList);
    this.categoriesList = [ ...categoriesList ];
    this.setState((state) => ({
      ...state,
      categories: categoriesList.filter(product => product.checked)
    }));
  }

  addStores = (storeList) => {
    console.log('addStores', storeList);
    this.storesList = [ ...storeList ];
    this.setState((state) => ({
      ...state,
      stores: storeList.filter(store => store.checked)
    }))
  }

  saveProfile = () => {
    console.log('saveProfile');
    const { navigation, actions } = this.props;
    const { saveProfile } = actions;
    const { dsName, categories, stores } = this.state;
    const profile = {
      dsName: dsName.trim(),
      categories,
      stores,
    };

    if (this.props.profiles.some(profile => profile.dsName === dsName.trim())) {
      ToastAndroid.show('Já existe um perfil com este nome!', ToastAndroid.SHORT);
    } else {
      saveProfile(profile)
        .then(() => {
          ToastAndroid.show('Perfil Salvo!', ToastAndroid.SHORT);
          setTimeout(() => {
            // testar se o goBack volta em prod
            actions.backScreen()
            navigation.goBack(null)
            // navigation.navigate('Main');
          }, 1250);

        })
        .catch(err => {
          ToastAndroid.show('Houve um problema ao salvar o seu perfil!', ToastAndroid.SHORT);
          console.log('err', err)
        });
    }
  }

  isValid = () => {
    const { dsName, categories, stores } = this.state;
    return !!dsName.length && !!categories.length && !!stores.length;
  }

  render() {
    return (
      this.props.savingProfile ?
      <Loader text='Salvando seu perfil...' /> :
      <Container scroll>
        <ScrollView>
          <View>
            <Text style={styles.listTitle}>Título do Perfil</Text>
            <TextInput
              onChangeText={(text) => this.setState({dsName: text})}
              value={this.state.dsName}
            />
          </View>
          <View style={styles.listContainer}>
            <Text style={styles.listTitle}>Categorias</Text>
            <ListItem items={this.categoriesList} action={this.addCategory} field="id" />
          </View>
          <View style={styles.listContainer}>
            <Text style={styles.listTitle}>Lojas</Text>
            <ListItem items={this.storesList} action={this.addStores} field="id" />
          </View>
          <View style={{marginTop: 20, marginBottom: 20}}>
            <Button
              onPress={this.saveProfile}
              disabled={!this.isValid()}
              title="Salvar"
              style={styles.btnSave}
            />
          </View>
        </ScrollView>
      </Container>
    );
  }
}

NewProfile.propTypes = {
  savingProfile: PropTypes.bool,
  currentScreen: PropTypes.string,
  profiles: PropTypes.array,
  categories: PropTypes.array,
  stores: PropTypes.array,
}

const mapStateToProps = ({ App, NewProfile }) => {
  const { savingProfile, profiles, categories, stores } = NewProfile;
  const { currentScreen } = App;

  return ({
    profiles,
    savingProfile,
    currentScreen,
    categories,
    stores,
  })
};

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(actions, dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(NewProfile);
