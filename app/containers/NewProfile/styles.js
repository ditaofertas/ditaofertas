import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  listContainer: {

  },
  listTitle: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  btnSave: {
    marginTop: 50,
  },
});

export default styles;
