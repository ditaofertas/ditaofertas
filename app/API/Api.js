import axios from 'axios';

const HOST = 'http://ditaofertas.herokuapp.com';

const toParams = (params) => {
  let urlParams = [];
  Object.keys(params).map((key) => {
    if (params[key].constructor === Array) {
      params[key].map((k) => {
        urlParams = [
          ...urlParams,
          `${key}[]=${k}`,
        ];
      });
    } else {
      urlParams = [
        ...urlParams,
        `${key}=${encodeURI(params[key])}`,
      ];
    }
    return urlParams;
  });
  return `?${urlParams.join('&')}`;
};

const getStores = () => axios.get(`${HOST}/getStores`);

const getCategories = () => axios.get(`${HOST}/getCategories`);

const getOffers = params => axios.get(`${HOST}/getOffers${toParams(params)}`);

export default {
  getStores,
  getCategories,
  getOffers,
};
