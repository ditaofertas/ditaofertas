import moment from 'moment';
import * as SQLiteHelper from '../modules/SQLiteConnection';

export const checkTableApp = () => new Promise((resolve, reject) => {
  const conn = SQLiteHelper.SQLiteConnection();
  conn.transaction((tx) => {
    tx.executeSql('SELECT name FROM sqlite_master WHERE type=\'table\' AND name=\'app\'', [], (tx, rs) => {
      const rows = rs.rows.raw();
      resolve(rows);
    }, (tx, error) => {
      reject({
        code: 406,
        msg: error,
      });
    });
  }, (error) => {
    reject({
      code: 406,
      msg: error,
    });
  });
});

export const checkTables = () => new Promise((resolve, reject) => {
  const conn = SQLiteHelper.SQLiteConnection();
  conn.transaction((tx) => {
    tx.executeSql('SELECT name FROM sqlite_master', [], (tx, rs) => {
      const rows = rs.rows.raw();
      resolve(rows);
    }, (tx, error) => {
      reject({
        code: 406,
        msg: error,
      });
    });
  }, (error) => {
    reject({
      code: 406,
      msg: error,
    });
  });
});

export const createAppTable = () => new Promise((resolve, reject) => {
  const conn = SQLiteHelper.SQLiteConnection();
  conn.transaction((tx) => {
    tx.executeSql(`CREATE TABLE IF NOT EXISTS app (
      createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
      configured INTEGER DEFAULT 0)`);
  }, (error) => {
    reject({
      code: 406,
      msg: error.message,
    });
  }, () => {
    resolve({
      code: 200,
      msg: 'TABLE App CREATED',
    });
  });
});

export const saveApp = configured => new Promise((resolve, reject) => {
  const conn = SQLiteHelper.SQLiteConnection();

  conn.transaction((tx) => {
    tx.executeSql(`INSERT INTO app(configured) VALUES (${configured})`);
  }, (error) => {
    console.error(error);
    reject({
      code: 406,
      msg: error,
    });
  }, () => {
    console.log('App configurada');
    resolve({
      code: 200,
      msg: 'App configurada',
    });
  });
});

export const selectApp = () => new Promise((resolve, reject) => {
  const conn = SQLiteHelper.SQLiteConnection();
  conn.transaction((tx) => {
    tx.executeSql('SELECT * FROM app', [], (tx, rs) => {
      const rows = rs.rows.raw();
      console.log('rows', rows);
      resolve(rows[0]);
    }, (tx, error) => {
      reject({
        code: 406,
        msg: error,
      });
    });
  }, (error) => {
    reject({
      code: 406,
      msg: error,
    });
  });
});

export const createStoreTable = () => new Promise((resolve, reject) => {
  const conn = SQLiteHelper.SQLiteConnection();
  conn.transaction((tx) => {
    tx.executeSql(`CREATE TABLE IF NOT EXISTS stores (
      id INTEGER,
      dsName TEXT)`);
  }, (error) => {
    reject({
      code: 406,
      msg: error.message,
    });
  }, () => {
    resolve({
      code: 200,
      msg: 'TABLE stores CREATED',
    });
  });
});


export const saveStores = stores => new Promise((resolve, reject) => {
  const conn = SQLiteHelper.SQLiteConnection();
  const storeValues = stores.map(({ id, dsName }) => `(${id}, '${dsName}')`).join();

  conn.transaction((tx) => {
    tx.executeSql(`INSERT INTO stores VALUES ${storeValues}`);
  }, (error) => {
    reject({
      code: 406,
      msg: error,
    });
  }, () => {
    resolve({
      code: 200,
      msg: 'Stores inseridas',
    });
  });
});

export const selectStores = () => new Promise((resolve, reject) => {
  const conn = SQLiteHelper.SQLiteConnection();
  conn.transaction((tx) => {
    tx.executeSql('SELECT * FROM stores', [], (tx, rs) => {
      const rows = rs.rows.raw();
      resolve(rows);
    }, (tx, error) => {
      reject({
        code: 406,
        msg: error.message,
      });
    });
  });
});

export const createCategoryTable = () => new Promise((resolve, reject) => {
  const conn = SQLiteHelper.SQLiteConnection();
  conn.transaction((tx) => {
    tx.executeSql(`CREATE TABLE IF NOT EXISTS categories (
      id INTEGER,
      dsName TEXT)`);
  }, (error) => {
    reject({
      code: 406,
      msg: error.message,
    });
  }, () => {
    resolve({
      code: 200,
      msg: 'Tabela categories criada',
    });
  });
});

export const saveCategories = categories => new Promise((resolve, reject) => {
  const conn = SQLiteHelper.SQLiteConnection();
  const storeValues = categories.map(({ id, dsName }) => `(${id}, '${dsName}')`).join();

  conn.transaction((tx) => {
    tx.executeSql(`INSERT INTO categories VALUES ${storeValues}`);
  }, (error) => {
    reject({
      code: 406,
      msg: error.message,
    });
  }, () => {
    resolve({
      code: 200,
      msg: 'Categorias inseridas',
    });
  });
});

export const selectCategories = () => new Promise((resolve, reject) => {
  const conn = SQLiteHelper.SQLiteConnection();
  conn.transaction((tx) => {
    tx.executeSql('SELECT * FROM categories', [], (tx, rs) => {
      const rows = rs.rows.raw();
      resolve(rows);
    }, (tx, error) => {
      reject({
        code: 406,
        msg: error.message,
      });
    });
  });
});

export const dropTableProfile = () => new Promise((resolve, reject) => {
  const conn = SQLiteHelper.SQLiteConnection();
  conn.transaction((tx) => {
    tx.executeSql('DROP TABLE profiles');
  }, (error) => {
    reject({
      code: 406,
      msg: error.message,
    });
  }, () => {
    resolve();
  });
});

export const dropTableApp = () => new Promise((resolve, reject) => {
  const conn = SQLiteHelper.SQLiteConnection();
  conn.transaction((tx) => {
    tx.executeSql('DROP TABLE app');
  }, (error) => {
    reject({
      code: 406,
      msg: error.message,
    });
  }, () => {
    resolve();
  });
});

export const dropTableCategories = () => new Promise((resolve, reject) => {
  const conn = SQLiteHelper.SQLiteConnection();
  conn.transaction((tx) => {
    tx.executeSql('DROP TABLE categories');
  }, (error) => {
    reject({
      code: 406,
      msg: error.message,
    });
  }, () => {
    resolve();
  });
});

export const dropTableStores = () => new Promise((resolve, reject) => {
  const conn = SQLiteHelper.SQLiteConnection();
  conn.transaction((tx) => {
    tx.executeSql('DROP TABLE stores');
  }, (error) => {
    reject({
      code: 406,
      msg: error.message,
    });
  }, () => {
    resolve();
  });
});

export const dropTablePastOffers = () => new Promise((resolve, reject) => {
  const conn = SQLiteHelper.SQLiteConnection();
  conn.transaction((tx) => {
    tx.executeSql('DROP TABLE pasto_ffers');
  }, (error) => {
    reject({
      code: 406,
      msg: error.message,
    });
  }, () => {
    resolve();
  });
});

export const createPastOffersTable = () => new Promise((resolve, reject) => {
  const conn = SQLiteHelper.SQLiteConnection();
  conn.transaction((tx) => {
    tx.executeSql(`CREATE TABLE IF NOT EXISTS past_offers (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      dsName TEXT NOT NULL,
      offers TEXT NOT NULL,
      searchedAt DATETIME NOT NULL)`);
  }, (error) => {
    reject({
      code: 406,
      msg: error.message,
    });
  }, () => {
    resolve();
  });
});

export const savePastOfferSQL = profile => new Promise((resolve, reject) => {
  const conn = SQLiteHelper.SQLiteConnection();
  const { dsName, offers } = profile;
  conn.transaction((tx) => {
    tx.executeSql(`INSERT INTO past_offers(dsName, offers, searchedAt) VALUES ('${dsName}', '${JSON.stringify(offers)}', '${moment()}')`);
  }, (error) => {
    reject({
      code: 406,
      msg: error.message,
    });
  }, () => {
    resolve();
  });
});

export const selectPastOffers = () => new Promise((resolve, reject) => {
  const conn = SQLiteHelper.SQLiteConnection();
  conn.transaction((tx) => {
    tx.executeSql('SELECT * FROM past_offers', [], (tx, rs) => {
      const rows = rs.rows.raw();
      console.log('pastOffers');
      console.table(rows);
      resolve(rows);
    }, (tx, error) => {
      reject({
        code: 406,
        msg: error,
      });
    });
  });
});

export const removePastOffers = pastOffersId => new Promise((resolve, reject) => {
  const conn = SQLiteHelper.SQLiteConnection();
  conn.transaction((tx) => {
    tx.executeSql(`DELETE FROM past_offers WHERE id = ${pastOffersId}`);
  }, (tx, error) => {
    console.error('error', error);
    reject({
      code: 406,
      msg: error,
    });
  }, () => {
    resolve();
  });
});

export const createProfileTable = () => new Promise((resolve, reject) => {
  const conn = SQLiteHelper.SQLiteConnection();
  conn.transaction((tx) => {
    tx.executeSql(`CREATE TABLE IF NOT EXISTS profiles (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      dsName TEXT NOT NULL,
      categories TEXT NOT NULL,
      stores TEXT NOT NULL)`);
  }, (error) => {
    reject({
      code: 406,
      msg: error.message,
    });
  }, () => {
    resolve();
  });
});

export const saveProfileSQL = profile => new Promise((resolve, reject) => {
  const conn = SQLiteHelper.SQLiteConnection();
  const { dsName, categories, stores } = profile;
  conn.transaction((tx) => {
    tx.executeSql(`INSERT INTO profiles(dsName, categories, stores)
    VALUES ('${dsName}','${JSON.stringify(categories)}','${JSON.stringify(stores)}')`);
  }, (error) => {
    reject({
      code: 406,
      msg: error.message,
    });
  }, () => {
    resolve();
  });
});

export const removeProfile = dsName => new Promise((resolve, reject) => {
  const conn = SQLiteHelper.SQLiteConnection();
  conn.transaction((tx) => {
    tx.executeSql(`DELETE FROM profiles WHERE dsName = '${dsName}'`);
  }, (tx, error) => {
    reject({
      code: 406,
      msg: error,
    });
  }, () => {
    resolve();
  });
});

export const selectProfiles = () => new Promise((resolve, reject) => {
  const conn = SQLiteHelper.SQLiteConnection();
  conn.transaction((tx) => {
    tx.executeSql('SELECT * FROM profiles', [], (tx, rs) => {
      const rows = rs.rows.raw();
      resolve(rows);
    }, (tx, error) => {
      reject({
        code: 406,
        msg: error.message,
      });
    });
  });
});
