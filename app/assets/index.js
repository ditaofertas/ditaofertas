
import profile from './images/profile.png';
import mktList from './images/mkt_list.png';
import stop from './images/stop.png';
import trash from './images/trash.png';

export default {
  images: {
    profile,
    mktList,
    stop,
    trash,
  },
};
