import {
  StackNavigator,
} from 'react-navigation';

import Main from './containers/Main';
import NewProfile from './containers/NewProfile';
import PastOffers from './containers/PastOffers';

const Navigation = StackNavigator({
  Main: {
    screen: Main,
    navigationOptions: () => ({
      title: 'Dita Ofertas - Main',
      visible: false,
    }),
  },
  NewProfile: { screen: NewProfile },
  PastOffers: { screen: PastOffers },
}, {
  headerMode: 'none',
});

export default Navigation;
