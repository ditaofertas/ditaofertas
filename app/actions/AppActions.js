import * as types from '../constants';
import * as sql from '../API/SQL';
import api from '../API/Api';

export const speakStarted = () => ({ type: types.SPEAK_STARTED });
export const speakFinished = () => ({ type: types.SPEAK_FINISHED });
export const speakStopped = () => ({ type: types.SPEAK_STOPPED });

export const speakStartedPastOffers = () => ({ type: types.SPEAK_STARTED_PAST_OFFERS });
export const speakFinishedPastOffers = () => ({ type: types.SPEAK_FINISHED_PAST_OFFERS });
export const speakStoppedPastOffers = () => ({ type: types.SPEAK_STOPPED_PAST_OFFERS });

export const connectionStatus = connected => ({
  type: types.CONNECTION_STATUS,
  payload: connected,
});

export const TTSConfigured = () => ({ type: types.TTS_CONFIGURED });

export const saveProfile = profile => (dispatch) => {
  dispatch({ type: types.SAVE_PROFILE });

  return sql.saveProfileSQL(profile)
    .then(() => dispatch({
      type: types.SAVE_PROFILE_SUCCESS,
      payload: profile,
    }))
    .catch((err) => {
      dispatch({
        type: types.SAVE_PROFILE_ERROR,
        payload: err,
      });
      throw err;
    });
};


export const removeProfile = (dsName, index) => (dispatch) => {
  dispatch({ type: types.REMOVE_PROFILE, payload: { dsName, index } });

  return sql.removeProfile(dsName)
    .then(() => dispatch({ type: types.REMOVE_PROFILE_SUCCESS, payload: { dsName, index } }))
    .catch((err) => {
      dispatch({
        type: types.REMOVE_PROFILE_ERROR,
        payload: err,
      });
      throw err;
    });
};

export const saveOffersSearched = profile => (dispatch) => {
  dispatch({ type: types.SAVE_OFFERS_SEARCHED });

  // Evitar registro duplicado no BD
  // depois ver possibilidade de remoção
  if (!!profile.offers.lenght) {
    return dispatch({ type: types.SAVE_OFFERS_SEARCHED_SUCCESS, payload: profile.offers });
  }

  return sql.savePastOfferSQL(profile)
    .then(() => dispatch({
      type: types.SAVE_OFFERS_SEARCHED_SUCCESS,
      payload: profile.offers,
    }))
    .catch((err) => {
      dispatch({
        type: types.SAVE_OFFERS_SEARCHED_ERROR,
        payload: err,
      });
      throw err;
    });
};

export const removeOffersSearched = profileID => (dispatch) => {
  dispatch({ type: types.REMOVE_OFFERS_SEARCHED, payload: profileID });

  return sql.removePastOffers(profileID)
    .then(() => dispatch({ type: types.REMOVE_OFFERS_SEARCHED_SUCCESS }))
    .catch((err) => {
      dispatch({
        type: types.REMOVE_OFFERS_SEARCHED_ERROR,
        payload: err,
      });
      throw err;
    });
};

export const fetchProfilesError = err => ({
  type: types.FETCH_PROFILES_ERROR,
  payload: err,
});

export const fetchProfilesSuccess = (profiles = []) => (dispatch) => {
  dispatch({
    type: types.FETCH_PROFILES_SUCCESS,
    payload: profiles,
  });
};

export const fetchProfiles = () => dispatch => new Promise((resolve, reject) => {
  dispatch({ type: types.FETCH_PROFILES });

  return sql.selectProfiles()
    .then((response) => {
      dispatch(fetchProfilesSuccess(response));
      resolve();
    })
    .catch((err) => {
      dispatch(fetchProfilesError(err));
      reject(err);
    });
});

export const fetchOffersError = err => ({
  type: types.FETCH_OFFERS_ERROR,
  payload: err,
});

export const fetchOffersSuccess = offers => ({ type: types.FETCH_OFFERS_SUCCESS, payload: offers.data });

export const fetchOffers = params => (dispatch) => {
  dispatch({ type: types.FETCH_OFFERS });

  return api.getOffers(params)
    .then((response) => {
      dispatch(fetchOffersSuccess(response));
      return response;
    })
    .catch((err) => {
      console.error(err);
      dispatch(fetchOffersError(err));
      throw err;
    });
};

export const navigateScreen = screen => ({ type: types.NAVIGATE_TO_SCREEN, payload: screen });

export const backScreen = () => ({ type: types.BACK_SCREEN });

export const appIsOnBackground = () => ({ type: types.APP_IS_ON_BACKGROUND });
