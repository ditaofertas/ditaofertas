
import * as types from '../constants';
import * as sql from '../API/SQL';
import { fetchProfiles } from './AppActions';
import api from '../API/Api';

export const createTablesError = err => ({
  type: types.CREATE_TABLES_ERROR,
  payload: err,
});

export const saveStores = err => ({
  type: types.CREATE_TABLE_STORES_ERROR,
  payload: err,
});

export const getCategoriesAndStoresError = err => ({
  type: types.FETCH_CATEGORIES_STORES_ERROR,
  payload: err,
});

export const startAppSuccess = () => ({ type: types.START_APP_SUCCESS });

export const startAppError = err => ({
  type: types.START_APP_ERROR,
  payload: err,
});

export const saveCategoriesAndStoresSuccess = payload => (dispatch) => {
  dispatch({
    type: types.SAVE_CATEGORIES_STORES_SUCCESS,
    payload,
  });

  sql.saveApp(1)
    .then(() => {
      dispatch(fetchProfiles())
        .then(() => dispatch(startAppSuccess()))
        .catch(err => dispatch(startAppError(err)));
    })
    .catch((err) => {
      dispatch(startAppError(err));
      throw err;
    });
};

export const saveCategoriesAndStores = ({ categories, stores }) => (dispatch) => {
  dispatch({ type: types.SAVE_CATEGORIES_STORES });

  const saveTableCategories = sql.saveCategories(categories.data);
  const saveTableStores = sql.saveStores(stores.data);

  return Promise.all([saveTableCategories, saveTableStores])
    .then(() => dispatch(saveCategoriesAndStoresSuccess({
      categories: categories.data,
      stores: stores.data,
    })))
    .catch((err) => {
      dispatch(getCategoriesAndStoresError(err));
      throw err;
    });
};

export const getCategoriesAndStoresFromApi = () => (dispatch) => {
  dispatch({ type: types.FETCH_CATEGORIES_STORES });

  const getCategories = api.getCategories();
  const getStores = api.getStores();

  return Promise.all([getCategories, getStores])
    .then(response => dispatch(saveCategoriesAndStores({
      categories: response[0].data,
      stores: response[1].data,
    })))
    .catch((err) => {
      dispatch(getCategoriesAndStoresError(err));
      throw err;
    });
};

export const getCategoriesAndStoresFromDB = () => (dispatch) => {
  dispatch({ type: types.FETCH_CATEGORIES_STORES_DB });

  const getCategories = sql.selectCategories();
  const getStores = sql.selectStores();

  return Promise.all([getCategories, getStores])
    .then(response => dispatch(saveCategoriesAndStoresSuccess({
      categories: response[0],
      stores: response[1],
    })))
    .catch((err) => {
      dispatch(getCategoriesAndStoresError(err));
      throw err;
    });
};

export const checkApp = () => (dispatch) => {
  dispatch({ type: types.FETCH_CATEGORIES_STORES });

  const getCategories = api.getCategories();
  const getStores = api.getStores();

  return Promise.all([getCategories, getStores])
    .then(response => dispatch(saveCategoriesAndStores({
      categories: response[0].data,
      stores: response[1].data,
    })))
    .catch((err) => {
      dispatch(getCategoriesAndStoresError(err));
      throw err;
    });
};

export const createTables = () => (dispatch) => {
  dispatch({ type: types.CREATE_TABLES });

  const createAppTable = sql.createAppTable();
  const createStoreTable = sql.createStoreTable();
  const createCategoryTable = sql.createCategoryTable();
  const createProfileTable = sql.createProfileTable();
  const createPastOffersTable = sql.createPastOffersTable();

  return Promise.all([
    createAppTable,
    createStoreTable,
    createCategoryTable,
    createProfileTable,
    createPastOffersTable])
    .then(() => dispatch(getCategoriesAndStoresFromApi()))
    .catch((err) => {
      dispatch(createTablesError(err));
      throw err;
    });
};

export const initApp = () => (dispatch) => {
  dispatch({ type: types.START_APP });

  return sql.checkTableApp()
    .then((response) => {
      if (response.length) {
        sql.selectApp()
          .then((res) => {
            if (res && res.configured) {
              dispatch(getCategoriesAndStoresFromDB());
            } else {
              dispatch(getCategoriesAndStoresFromApi());
            }
          })
          .catch((err) => {
            console.error(err);
            dispatch(startAppError(err));
            throw err;
          });
      } else {
        dispatch(createTables());
      }
    })
    .catch((err) => {
      console.error(err);
      dispatch(startAppError(err));
      throw err;
    });
};
