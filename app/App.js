import React, { Component } from 'react';
import { BackAndroid, AppState } from 'react-native';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'remote-redux-devtools';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger'

import { initApp } from './actions/ConfigurationActions';
import { backScreen, appIsOnBackground } from './actions/AppActions';
import reducer from './reducers';
import Navigation from './Navigation';

const composeEnhancers = composeWithDevTools({ realtime: true });
const store = createStore(reducer, composeEnhancers(
  applyMiddleware(thunk, createLogger({
    diff: true,
    duration: true,
  })),
));

class DitaOfertas extends Component {

  componentWillMount() {
    store.dispatch(initApp());
  }

  componentDidMount() {
    BackAndroid.addEventListener('hardwareBackPress', () => {
      if (store.getState().currentScreen === 'Main') {
        BackAndroid.exitApp();
      } else {
        store.dispatch(backScreen());
      }
    });

    AppState.addEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = (nextAppState) => {
    if (nextAppState === 'background') {
      console.log('App has come to the background!')
      store.dispatch(appIsOnBackground());
    }
  }

  render() {
    return (
      <Provider store={store}>
        <Navigation />
      </Provider>
    );
  }
}

export default DitaOfertas;
