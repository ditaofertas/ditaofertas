package com.speechToFile;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import java.io.File;
import android.os.Environment;

import android.widget.Toast;
import android.speech.tts.TextToSpeech;

public class SpeechToFileModule extends ReactContextBaseJavaModule {

  private TextToSpeech myTTS;
  private boolean ready;

  public SpeechToFileModule(ReactApplicationContext reactContext) {
    super(reactContext);

    myTTS = new TextToSpeech(getReactApplicationContext(), new TextToSpeech.OnInitListener() {
                @Override
                public void onInit(int status) {
                    if (status != TextToSpeech.SUCCESS) {
                        ready = false;
                    } else {
                        ready = true;
                    }
                }
            });

  }

  @Override
  public String getName() {
    return "SpeechToFileModuleAndroid";
  }

  @ReactMethod
  public void save() {
    CharSequence wakeUpText = "Smartphone Moto G 4 Play DTV Colors Dual Chip Android 6.0 Tela 5'' 16GB Câmera 8MP - Preto R$ 256,35";
    String path = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "wakeUp.mp3";
    System.out.println("Path" + path);
    File file = new File(path);
    int req = myTTS.synthesizeToFile(wakeUpText , null, file, "Req");
    if(req == TextToSpeech.SUCCESS) {
      Toast toast = Toast.makeText(getReactApplicationContext(), "Saved "+req, Toast.LENGTH_SHORT);
      toast.show();
    }
  }
}
