//https://github.com/zmxv/react-native-sound
/**
 * Sample React Native App

AppRegistry.registerComponent('DitaOfertas', () => DitaOfertas);
 * https://github.com/facebook/react-native
 * @flow
 */
import React from 'react-native';
import DitaOfertas from './app/App';

const {
  AppRegistry,
} = React;

AppRegistry.registerComponent('DitaOfertas', () => DitaOfertas);
